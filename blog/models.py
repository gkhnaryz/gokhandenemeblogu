from django.db import models
from django.utils import timezone
from proje.settings import AUTH_USER_MODEL


class Post(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    title = models.CharField(max_length=200)
    text = models.TextField(help_text="bu alan")
    image = models.ImageField(upload_to='kullanici_resimleri/%Y/%m/%d/', default='kullanici_resimleri/kullanici_resmi.jpg')
    video = models.FileField(upload_to = u'video/', max_length=200)
    created_date = models.DateTimeField(auto_now=True,)
    published_date = models.DateTimeField(auto_now=True,)
    updated_date = models.DateTimeField(auto_now=True,)
    #slug  = models.SlugField(max_length=80, null=True, blank=True, help_text=u"Link, otomatik alinir, değiştirmeyiniz!!!")

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

        