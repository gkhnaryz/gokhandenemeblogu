from django.contrib import admin

# Register your models here.

from .models import Post

class PostModelAdmin(admin.ModelAdmin):
    list_display = ["title", "published_date", "updated_date"]
    list_display_links = ["updated_date"]
    list_editable = ["title"]
    list_filter = ["updated_date", "published_date"]
    search_fields = ["title", "text"]
class Meta:
    model = Post


#class PostModelAdmin(admin.ModelAdmin):
 #   prepopulated_fields = {"slug": ("title",)}

admin.site.register(Post, PostModelAdmin)