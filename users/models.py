from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):

    adres = models.CharField(max_length=250,null=True, blank=True) #maks 255 karakter
    tel = models.CharField(max_length=250)
    bio = models.TextField()
    resim = models.ImageField(upload_to='kullanici_resimleri/%Y/%m/%d/', default='kullanici_resimleri/kullanici_resmi.jpg')
    git = models.URLField()
    